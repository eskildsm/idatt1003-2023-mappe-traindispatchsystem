package edu.ntnu.stud;

/** This is the main class for the train dispatch application. */
public class TrainDispatchApp {
  /**
   * Main method for the train dispatch application.
   *
   * @param args command line arguments
   */
  public static void main(String[] args) {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    UI ui = new UI(trainDepartureRegister);
    ui.start();
  }
}
