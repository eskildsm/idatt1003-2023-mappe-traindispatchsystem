package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.Scanner;

/** This is the main class for the train dispatch application. */
public class UI {
  TrainDepartureRegister trainDepartureRegister;

  private Scanner choice;

  UI(TrainDepartureRegister trainDepartureRegister) {
    this.trainDepartureRegister = trainDepartureRegister;

    this.choice = new Scanner(System.in);
  }

  /** Method shows the menu to the user. */
  private void showMenu() {
    System.out.println("1. Add train departure");
    System.out.println("2. Delete train departure");
    System.out.println("3. Update values for train departure");
    System.out.println("4. Get all train departures");
    System.out.println("5. Get train departure by departure number");
    System.out.println("6. Get train departures by destination");
    System.out.println("7. Update time of day");
    System.out.println("8. Exit");
  }

  /** Method prints out submenu to console. */
  private void showSubMenu() {
    System.out.println("1. Update departure time");
    System.out.println("2. Update track number");
    System.out.println("3. Back");
  }

  /** Method adds a train departure to the train departure register. */
  private int getChoice() {
    this.choice.reset();
    System.out.print("Enter choice: ");
    try {
      int choice = this.choice.nextInt();
      System.out.println(choice);
      return choice;
    } catch (NumberFormatException e) {
      System.out.println("Invalid input, enter integer!");
    }
    return 0;
  }

  /** Method adds a train departure to the train departure register. */
  private void addTrainDeparture() {
    this.choice.reset();
    try {
      System.out.println("Crate new train departure: ");

      System.out.println("Enter departure number (required): ");
      int departureNumber = this.choice.nextInt();

      System.out.println("Enter line (required): ");
      String line = this.choice.next();

      System.out.println("Enter destination (required): ");
      String destination = this.choice.next();

      System.out.println("Enter departure time (optional): ");
      String timeString = this.choice.next();

      System.out.println("Enter track number (optional): ");
      int trackNumber = this.choice.nextInt();

      this.trainDepartureRegister.createTrainDeparture(departureNumber, destination, line);

      this.trainDepartureRegister.setDepartureTimeForDeparture(departureNumber, timeString);
      this.trainDepartureRegister.setTrackForDeparture(departureNumber, trackNumber);

    } catch (NumberFormatException e) {
      System.out.println("Invalid input");
    }
  }

  /** Method deletes the train departure with the given departure number. */
  private void deleteTrainDeparture() {
    this.choice.reset();
    try {
      System.out.println("Enter departure number: ");
      int departureNumber = this.choice.nextInt();

      this.trainDepartureRegister.removeTrainDeparture(departureNumber);
    } catch (NumberFormatException e) {
      System.out.println("Invalid input");
    }
  }

  private void setDepartureTime() {
    this.choice.reset();
    try {
      System.out.println("Enter departure number: ");
      int departureNumber = this.choice.nextInt();

      System.out.println("Enter departure time: ");
      String timeString = this.choice.next();

      this.trainDepartureRegister.setDepartureTimeForDeparture(departureNumber, timeString);
    } catch (NumberFormatException e) {
      System.out.println("Invalid input");
    }
  }

  /** Method writes all train departures to the console. */
  private void setTrackNumber() {
    this.choice.reset();
    try {
      System.out.println("Enter departure number: ");
      int departureNumber = this.choice.nextInt();

      System.out.println("Enter track number: ");
      int trackNumber = this.choice.nextInt();

      this.trainDepartureRegister.setTrackForDeparture(departureNumber, trackNumber);
    } catch (NumberFormatException e) {
      System.out.println("Invalid input");
    }
  }

  /** Method writes all train departures to the console. */
  private void getAllTrainDepartures() {
    System.out.println(this.trainDepartureRegister);
  }

  /** Method writes the train departure with the given departure number to the console. */
  private void getTrainDepartureByDepartureNumber() {
    this.choice.reset();
    try {
      System.out.println("Enter departure number: ");
      int departureNumber = this.choice.nextInt();

      System.out.println(this.trainDepartureRegister.searchForTrainDeparture(departureNumber));
    } catch (NumberFormatException e) {
      System.out.println("Invalid input");
    }
  }

  /** Method writes all train departures with the given destination to the console. */
  private void getTrainDeparturesByDestination() {
    this.choice.reset();
    try {
      System.out.println("Enter destination: ");
      String destination = this.choice.next();

      System.out.println(this.trainDepartureRegister.searchForTrainDeparture(destination));
    } catch (NumberFormatException e) {
      System.out.println("Invalid input");
    }
  }

  /**
   * Method executes user choice.
   *
   * @param choice user choice
   * @param menu menuIndex
   */
  private void executeChoice(int choice, int menu) {
    if (menu == 0) {
      switch (choice) {
        case 1:
          addTrainDeparture();
          break;
        case 2:
          deleteTrainDeparture();
          break;
        case 3:
          updateValuesSubMenu();
          break;
        case 4:
          getAllTrainDepartures();
          break;
        case 5:
          getTrainDepartureByDepartureNumber();
          break;
        case 6:
          getTrainDeparturesByDestination();
          break;
        case 7:
          setDepartureTime();
          break;
        case 8:
          System.exit(0);
          break;
        default:
          System.out.println("Invalid input");
          break;
      }
    } else {
      switch (choice) {
        case 1:
          setDepartureTime();
          break;
        case 2:
          setTrackNumber();
          break;
        case 3:
          break;
        default:
          System.out.println("Invalid input");
          break;
      }
    }
  }

  /**
   * Method activates submenu for updating the values for the train departure with the given
   * departure number.
   */
  private void updateValuesSubMenu() {
    showSubMenu();
    int choice = getChoice();
    executeChoice(choice, 1);
  }

  /**
   * Method starts the application. This method contains code only used for testing purposes. The
   * method is called after init method
   */
  public void start() {
    while (true) {
      showMenu();
      int choice = getChoice();
      executeChoice(choice, 0);
    }
  }
}
