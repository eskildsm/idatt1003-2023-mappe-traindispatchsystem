package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.ArrayList;

public class TrainDepartureRegister {
    HashMap<Integer, TrainDeparture> trainDeparturesKeyedWithTrainDepartureNumber;
    HashMap<String, ArrayList<TrainDeparture>> trainDeparturesKeyedWithDestination;

    public TrainDepartureRegister() {
        trainDeparturesKeyedWithTrainDepartureNumber = new HashMap<>();
        trainDeparturesKeyedWithDestination = new HashMap<>();
    }

    private void addTrainDeparture(TrainDeparture newTrainDeparture) throws IllegalArgumentException {
        if(trainDeparturesKeyedWithTrainDepartureNumber.containsKey(newTrainDeparture.getTrainDepartureNumber())) {
            throw new IllegalArgumentException("Train departure number already exists");
        }

        trainDeparturesKeyedWithTrainDepartureNumber.put(newTrainDeparture.getTrainDepartureNumber(), newTrainDeparture);
        trainDeparturesKeyedWithDestination.putIfAbsent(newTrainDeparture.getDestination(), new ArrayList<>());
        trainDeparturesKeyedWithDestination.get(newTrainDeparture.getDestination()).add(newTrainDeparture);
    }

    public void createTrainDeparture(int trainDepartureNumber, String line, String destination) {
        TrainDeparture trainDeparture = new TrainDeparture(trainDepartureNumber, line, destination);
        addTrainDeparture(trainDeparture);
    }

    public void setTrackForDeparture(int trainDepartureNumber, int track) throws IllegalArgumentException {
        if(trainDeparturesKeyedWithTrainDepartureNumber.containsKey(trainDepartureNumber)) {
            trainDeparturesKeyedWithTrainDepartureNumber.get(trainDepartureNumber).setTrack(track);
        }
        else {
            throw new IllegalArgumentException("Train departure number does not exist");
        }
    }

    public void setDepartureTimeForDeparture(int trainDepartureNumber, String departureTime) {
        if(trainDeparturesKeyedWithTrainDepartureNumber.containsKey(trainDepartureNumber)) {
            trainDeparturesKeyedWithTrainDepartureNumber.get(trainDepartureNumber).setDepartureTime(LocalTime.parse(departureTime));
        }
        else {
            throw new IllegalArgumentException("Train departure number does not exist");
        }
    }

    public void setDelayForDeparture(int trainDepartureNumber, String delay) {
        if(trainDeparturesKeyedWithTrainDepartureNumber.containsKey(trainDepartureNumber)) {
            trainDeparturesKeyedWithTrainDepartureNumber.get(trainDepartureNumber).setDelay(LocalTime.parse(delay));
        }
        else {
            throw new IllegalArgumentException("Train departure number does not exist");
        }
    }

    public String searchForTrainDeparture(int trainDepartureNumber) throws IllegalArgumentException {
        if(trainDeparturesKeyedWithTrainDepartureNumber.containsKey(trainDepartureNumber)) {
            return trainDeparturesKeyedWithTrainDepartureNumber.get(trainDepartureNumber).toString();
        } else {
            throw new IllegalArgumentException("Train departure number does not exist");
        }
    }

    public String searchForTrainDeparture(String destination) throws IllegalArgumentException {
        if(trainDeparturesKeyedWithDestination.containsKey(destination)) {
            String result = "";
            for (TrainDeparture trainDeparture : trainDeparturesKeyedWithDestination.get(destination)) {
                result += trainDeparture.toString() + "\n";
            }
            return result;
        } else {
            throw new IllegalArgumentException("Destination does not exist");
        }
    }

    public void removeTrainDeparture(int trainDepartureNumber) {
        if(trainDeparturesKeyedWithTrainDepartureNumber.containsKey(trainDepartureNumber)) {
            TrainDeparture trainDeparture = trainDeparturesKeyedWithTrainDepartureNumber.get(trainDepartureNumber);
            trainDeparturesKeyedWithTrainDepartureNumber.remove(trainDepartureNumber);
            trainDeparturesKeyedWithDestination.remove(trainDeparture.getDestination());
        }
    }

    public ArrayList<String> getTrainDepartures() {
        return trainDeparturesKeyedWithTrainDepartureNumber.
                entrySet().stream().
                sorted((trainDeparture1, trainDeparture2)->{
                    return trainDeparture1.getValue().getDepartureTime().compareTo(trainDeparture2.getValue().getDepartureTime());
                }).collect(ArrayList::new, (list, trainDeparture) -> {
                    list.add(trainDeparture.getValue().toString());
                }, ArrayList::addAll);
    }

    @Override
    public String toString() {
        ArrayList<String> sortedTrainDepartureStrings = getTrainDepartures();

        StringBuilder result = new StringBuilder();
        for (String trainDepartureString : sortedTrainDepartureStrings) {
            result.append(trainDepartureString).append("\n");
        }
        return result.toString();
    }
}
