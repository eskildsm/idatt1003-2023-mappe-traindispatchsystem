package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * This class is used to store data regarding a train departure It is used by another class to keep
 * track over: train departures trainDepartureNumber variable is final because this instances of
 * this class is linked to one train, and trainDepartureNumber should therefore not change If values
 * are not set, they become null, and is translated to the user as "unknown" in toString.
 *
 * @author Eskild Smestu
 * @version 1.0
 * @since 2020-02-10
 */
public class TrainDeparture {
  private LocalTime departureTime;
  private final String line;
  private final int trainDepartureNumber;
  private final String destination;
  private int track;
  private LocalTime delay;

  /**
   * This constructor is used to create a new instance of TrainDeparture, with trainDepartureNumber
   * as parameter.
   *
   * @param trainDepartureNumber the number of the train departure
   * @throws IllegalArgumentException if trainDepartureNumber is negative or zero, or if line or
   *     destination is empty
   */
  public TrainDeparture(int trainDepartureNumber, String line, String destination)
      throws IllegalArgumentException {
    if (trainDepartureNumber < 0) {
      throw new IllegalArgumentException("Train number must be positive and non-zero");
    }

    if (line.isEmpty()) {
      throw new IllegalArgumentException("Line must be set");
    }

    if (destination.isEmpty()) {
      throw new IllegalArgumentException("Destination must be set");
    }

    this.track = -1;
    this.line = line;
    this.destination = destination;
    this.delay = LocalTime.parse("00:00");
    this.trainDepartureNumber = trainDepartureNumber;
  }

  /**
   * This method is used to set the departure time of the train.
   *
   * @param departureTime the time the train is scheduled to depart
   * @throws IllegalArgumentException if departureTime is null
   */
  public void setDepartureTime(LocalTime departureTime) throws IllegalArgumentException {
    if (departureTime == null) {
      throw new IllegalArgumentException("Departure time must be set");
    }

    this.departureTime = departureTime;
  }

  public void setTrack(int track) {
    this.track = track;
  }

  /**
   * This method is used to set the delay of the train.
   *
   * @param delay the delay of the train, default is 00:00
   * @throws IllegalArgumentException if delay is null
   */
  public void setDelay(LocalTime delay) throws IllegalArgumentException {
    if (delay == null) {
      throw new IllegalArgumentException("Delay must be set");
    }

    this.delay = delay;
  }

  public LocalTime getDepartureTime() {
    return departureTime;
  }

  public String getLine() {
    return line;
  }

  public int getTrainDepartureNumber() {
    return trainDepartureNumber;
  }

  public String getDestination() {
    return destination;
  }

  public int getTrack() {
    return track;
  }

  public LocalTime getDelay() {
    return delay;
  }

  /**
   * This method is used to print the values of the train departure.
   *
   * @return a string containing the values of the train departure and values will be "unknown" if
   *     not set
   */
  @Override
  public String toString() {
    return "Departure Time="
        + (this.departureTime == null ? "unknown" : this.departureTime.toString())
        + ", Line='"
        + (this.line == null ? "unknown" : this.line)
        + '\''
        + ", Train Departure Number="
        + (this.trainDepartureNumber == -1 ? "unknown" : this.trainDepartureNumber)
        + ", Destination='"
        + (this.destination == null ? "unknown" : this.destination)
        + '\''
        + ", Track="
        + (this.track == -1 ? "unknown" : this.track)
        + ", Delay="
        + this.delay.toString();
  }
}
