package edu.ntnu.stud;

import java.time.LocalTime;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TrainDepartureTest {
    @Test
    void testTrainDepartureConstructorTrainDepartureNumberEquals0TrackEqualsMinus1AndDelayEquals00_00() {
        TrainDeparture trainDeparture = new TrainDeparture(0, "L1", "Oslo");
        assertEquals(0, trainDeparture.getTrainDepartureNumber());
        assertEquals("L1", trainDeparture.getLine());
        assertEquals("Oslo", trainDeparture.getDestination());
        assertEquals(-1, trainDeparture.getTrack());
        assertEquals(LocalTime.parse("00:00"), trainDeparture.getDelay());
    }

    @Test
    void testTrainDepartureConstructorTrainDepartureNumberEqualsMinus1ThrowsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(-1, "L1", "Oslo"));
    }

    @Test
    void testTrainDepartureConstructorLineIsEmptyThrowsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(1, "", "Oslo"));
    }

    @Test
    void testTrainDepartureConstructorDestinationIsEmptyThrowsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(1, "L1", ""));
    }

    @Test
    void testTrainDepartureConstructorNegativeTrainDepartureNumber() {
        assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(-1, "L1", "Oslo"));
    }

    @Test
    void testSetDepartureTimeTo01_00() {
        TrainDeparture trainDeparture = new TrainDeparture(1, "L1", "Oslo");
        trainDeparture.setDepartureTime(LocalTime.parse("01:00"));
        assertEquals(LocalTime.parse("01:00"), trainDeparture.getDepartureTime());
    }

    @Test
    void testSetDepartureTimeToNull() {
        TrainDeparture trainDeparture = new TrainDeparture(1, "L1", "Oslo");
        assertThrows(IllegalArgumentException.class, () -> trainDeparture.setDepartureTime(null));
    }

    @Test
    void testSetTrackTo1() {
        TrainDeparture trainDeparture = new TrainDeparture(1, "L1", "Oslo");
        trainDeparture.setTrack(1);
        assertEquals(1, trainDeparture.getTrack());
    }


    @Test
    void testSetDelayTo01_00() {
        TrainDeparture trainDeparture = new TrainDeparture(1, "L1", "Oslo");
        trainDeparture.setDelay(LocalTime.parse("01:00"));
        assertEquals(LocalTime.parse("01:00"), trainDeparture.getDelay());
    }

    @Test
    void testSetDelayToNull() {
        TrainDeparture trainDeparture = new TrainDeparture(1, "L1", "Oslo");
        assertThrows(IllegalArgumentException.class, () -> trainDeparture.setDelay(null));
    }

    @Test
    void testToStringWithValidValues() {
        TrainDeparture trainDeparture = new TrainDeparture(1, "L1", "Oslo");
        trainDeparture.setDepartureTime(LocalTime.parse("00:00"));
        trainDeparture.setTrack(1);
        trainDeparture.setDelay(LocalTime.parse("00:01"));
        assertEquals("Departure Time=00:00, Line='L1', Train Departure Number=1, Destination='Oslo', Track=1, Delay=00:01", trainDeparture.toString());
    }

    @Test
    void testToStringWithNullValues() {
        TrainDeparture trainDeparture = new TrainDeparture(1, "L1", "Oslo");
        assertEquals("Departure Time=unknown, Line='L1', Train Departure Number=1, Destination='Oslo', Track=unknown, Delay=00:00", trainDeparture.toString());
    }
}
