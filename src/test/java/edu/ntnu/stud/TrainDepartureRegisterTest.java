package edu.ntnu.stud;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TrainDepartureRegisterTest {
  @Test
  void testTrainDepartureRegisterConstructorInitializesEmpty() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    assertEquals(0, trainDepartureRegister.getTrainDepartures().size());
  }

  @Test
  void testCreateTrainDeparture() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg");
    assertEquals(1, trainDepartureRegister.getTrainDepartures().size());
    //assertEquals(1, trainDepartureRegister.toString().get(0).getTrainDepartureNumber());
  }

  @Test
  void testCreateTrainDepartureWithTwoIdentical() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg");
    assertThrows(
        IllegalArgumentException.class, () -> trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg"));
  }

  @Test
  void testSearchForTrainDepartureWithTrainDepartureNumber() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg");
    assertEquals(
        "Departure Time=unknown, Line='L1', Train Departure Number=1, Destination='Kongsberg', Track=unknown, Delay=00:00",
        trainDepartureRegister.searchForTrainDeparture(1));
  }

  @Test
  void testSearchForNonExistingTrainDeparture() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    assertThrows(
        IllegalArgumentException.class, () -> trainDepartureRegister.searchForTrainDeparture(1));
  }

  @Test
  void testSetTrackForDeparture() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg");
    trainDepartureRegister.setTrackForDeparture(1, 1);
    assertEquals("Departure Time=unknown, Line='L1', Train Departure Number=1, Destination='Kongsberg', Track=1, Delay=00:00\n", trainDepartureRegister.toString());
  }

  @Test
  void testSetTrackForDepartureWithInvalidTrainDepartureNumber() {
      TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
      trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg");
      assertThrows(
          IllegalArgumentException.class, () -> trainDepartureRegister.setTrackForDeparture(2, 1));
  }

  @Test
  void testSetDepartureTimeForDeparture() {
      TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
      trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg");
      trainDepartureRegister.setDepartureTimeForDeparture(1, "00:00");
      assertEquals("Departure Time=00:00, Line='L1', Train Departure Number=1, Destination='Kongsberg', Track=unknown, Delay=00:00\n", trainDepartureRegister.toString());
  }

  @Test
  void testSetDepartureTimeForDepartureWithInvalidTrainDepartureNumber() {
      TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
      trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg");
      assertThrows(
          IllegalArgumentException.class, () -> trainDepartureRegister.setDepartureTimeForDeparture(2, "00:00"));
  }

  @Test
  void testSetDelayForDeparture() {
      TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
      trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg");
      trainDepartureRegister.setDelayForDeparture(1, "00:00");
      assertEquals("Departure Time=unknown, Line='L1', Train Departure Number=1, Destination='Kongsberg', Track=unknown, Delay=00:00\n", trainDepartureRegister.toString());
  }

  @Test
  void testSetDelayForDepartureWithInvalidTrainDepartureNumber() {
      TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
      trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg");
      assertThrows(
          IllegalArgumentException.class, () -> trainDepartureRegister.setDelayForDeparture(2, "00:00"));
  }

  @Test
  void testSearchForTrainDepartureWithDestination() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg");

    assertEquals(
        "Departure Time=unknown, Line='L1', Train Departure Number=1, Destination='Kongsberg', Track=unknown, Delay=00:00\n",
        trainDepartureRegister.searchForTrainDeparture("Kongsberg"));
  }

  @Test
    void testSearchForNonExistingDestination() {
        TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
        assertThrows(
            IllegalArgumentException.class, () -> trainDepartureRegister.searchForTrainDeparture("Kongsberg"));
    }

  @Test
  void testSearchForTrainDepartureWithDestinationMultiple() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg");
    trainDepartureRegister.createTrainDeparture(2, "L1", "Kongsberg");

    assertEquals(
        "Departure Time=unknown, Line='L1', Train Departure Number=1, Destination='Kongsberg', Track=unknown, Delay=00:00\n"
            + "Departure Time=unknown, Line='L1', Train Departure Number=2, Destination='Kongsberg', Track=unknown, Delay=00:00\n",
        trainDepartureRegister.searchForTrainDeparture("Kongsberg"));
  }

  @Test
  void testRemoveTrainDeparture() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg");
    trainDepartureRegister.createTrainDeparture(2, "L1", "Kongsberg");
    trainDepartureRegister.removeTrainDeparture(1);
    assertEquals(1, trainDepartureRegister.getTrainDepartures().size());
    assertEquals("Departure Time=unknown, Line='L1', Train Departure Number=2, Destination='Kongsberg', Track=unknown, Delay=00:00\n", trainDepartureRegister.toString());
  }

  @Test
  void testToString() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    trainDepartureRegister.createTrainDeparture(1, "L1", "Kongsberg");
    trainDepartureRegister.createTrainDeparture(2, "L1", "Kongsberg");

    trainDepartureRegister.setDepartureTimeForDeparture(1, "00:00");
    trainDepartureRegister.setDepartureTimeForDeparture(2, "00:01");

    assertEquals(
        "Departure Time=00:00, Line='L1', Train Departure Number=1, Destination='Kongsberg', Track=unknown, Delay=00:00\n"
            + "Departure Time=00:01, Line='L1', Train Departure Number=2, Destination='Kongsberg', Track=unknown, Delay=00:00\n",
        trainDepartureRegister.toString());
  }
}
